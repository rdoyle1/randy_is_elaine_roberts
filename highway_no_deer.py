from car import *
import random
import matplotlib.pyplot as plott
import matplotlib.animation as anim
import time as TIME
cars = []
num_cars = 0
time = 0
dt=1
highwaylength=1000

printGraph=1

if printGraph:
    fig=plott.figure()
    ax1=fig.add_subplot(1,1,1)
    plott.ion()

    plott.show()

while num_cars<1001:     #Doesn't work properly when removing cars if < 1000 or <= 1000 is used
    time += 1

    if num_cars < highwaylength:
        cars.append(car())
        num_cars += 1

    cars_to_remove=[]
    for i in range(len(cars)):
        if cars[i].get_pos() >= 100.0:    #With just >, there'd be a max of 101 cars on the highway
            cars_to_remove.append(i)
        else:
            cars[i].update(dt)
    u = 0
    if len(cars_to_remove):
        for n in cars_to_remove:
            del cars[(n-u)]
            u += 1

    if len(cars) == 0:
        print "broke"
        break

    if printGraph:
        plott.clf()

        plott.plot([b.get_pos()*5 for b in cars],[5]*len(cars),"b.")
        plott.plot([0,40],[0,10],c="white")
        TIME.sleep(.1)

        plott.draw()


print cars
print num_cars
print time
